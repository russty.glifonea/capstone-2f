const mongoose = require('mongoose')

const product_schema = new mongoose.Schema({
	name: {
		type: String,
		require: [true, 'Product name is required.']
	},
	description: {
		type: String,
		required: [true, 'Description is required.']
	},
	price: {
		type: Number,
		required: [true, 'Price is required.']
	},
	color: {
		type: String,
		required: [true, 'Color is required.']
	},
	sizes: {
		type: String,
		required: [ true, 'Size is required.']
	},
	quantity: {
		type: Number,
		required: [ true, 'Quantity is required.']
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	order: [
	{
		userId: {
			type: String,
			required: [true, 'UserID is Required.']
		},
		orderedOn: {
			type: Date,
			default: new Date()
		}
	}]
})

module.exports = mongoose.model('Product', product_schema)
