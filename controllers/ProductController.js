const Product = require('../models/Product')

module.exports.addProduct = (data) => {
	if(data.isAdmin){
		let new_product = new Product({
		name: data.product.name,
		description: data.product.description,
		color: data.product.color,
		sizes: data.product.sizes,
		quantity: data.product.quantity,
		price: data.product.price
	})

	return new_product.save().then((new_product, error) => {
		if(error){
			return false
		}

		return {
			message: 'New product successfully created!'
		}
	})
}
	
	let message = Promise.resolve('User must be ADMIN to access this.')

	return message.then((value) => {
		return value
	})
	
}

module.exports.getAllProducts = () => {
	return Product.find({}).then((result) => {
		return result
	})
}

module.exports.getAllActive = () => {
	return Product.find({isActive: true}).then((result) => {
		return result
	})
}

module.exports.getProduct = (product_id) => {
	return Product.findById(product_id).then((result) => {
		return result
	})
}

module.exports.updateProduct = (product_id, new_data) => {
	return Product.findByIdAndUpdate(product_id, {
		name: new_data.name,
		description: new_data.description,
		color: new_data.color,
		sizes: new_data.sizes,
		quantity: new_data.quantity,
		price: new_data.price
	}).then((update_product, error) => {
		if(error){
			return false
		}
		return {
			message: 'Product has been updated successfully'
		}
	}) 
}

module.exports.getProductArchive = (product_id, new_data) => {
	return Product.findByIdAndUpdate(product_id, {
		isActive: false
	}).then((update_product, error) => {
		if(error){
			return false
		}
		return {
			message: 'The product has been archived successfully'
		}
	}) 
}