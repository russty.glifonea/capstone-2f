const User = require('../models/User')
const Product = require('../models/Product')
const bcrypt = require('bcrypt')
const auth = require('../auth.js')

module.exports.checkIfEmailExists = (data) => {
	return User.find({email: data.email}).then((result) => {
		if(result.length > 0){
		return true
	}
	return false
})
}

module.exports.register = (data) => {
	let encrypted_password = bcrypt.hashSync(data.password, 10)

	let new_user = new User({
		firstName: data.firstName,
		lastName: data.lastName,
		email: data.email,
		mobileNo: data.mobileNo,
		password: encrypted_password
	})

	return new_user.save().then((created_user, error) => {
		if(error){
			return error
		}

		return {
			message: 'User successfully registered!'
		}
	})
} 

module.exports.login = (data) => {
	return User.findOne({email: data.email}).then((result) => {
		if(result == null){
			return {
				message: "User doesn't exist!"
			}
		}

		const is_password_correct = bcrypt.compareSync(data.password, result.password)

		if(is_password_correct) {
			return { 
				accessToken: auth.createAccessToken(result)
			}
		}

		return {
			message: 'Password is incorrect!'
		}
	})
}

module.exports.getUserDetails = (user_id) => {
	return User.findById(user_id, {password: 0}).then((result) => {
		return result
	})
}

module.exports.updateUser = (user_id, new_data) => {
	return User.findByIdAndUpdate(user_id, {
		firstName: new_data.firstName,
		lastName: new_data.lastName,
		email: new_data.email,
		password: new_data.password,
		mobileNo: new_data.mobileNo,
		isAdmin: new_data.isAdmin
	}).then((update_user, error) => {
		if(error){
			return false
		}
		return {
			message: 'User has been updated successfully'
		}
	}) 
}

module.exports.order = async (data) => {
    let is_user_updated = await User.findById(data.userId).then((user) => {
        user.orders.push({
            productId: data.productId
        })

        return user.save().then((updated_user, error) => {
            if(error){
                return false
            }

            return true
        })
    })


    let is_product_updated = await Product.findById(data.productId).then((product) => {
      	product.order.push({
            userId: data.userId
        })

        return product.save().then((updated_product, error) => {
            if(error){
                return false
            }

            return true
        })
    })

    
    if(is_user_updated && is_product_updated){
        return {
            message: 'User order is successful!'
        }
    }

    //  If the enrollment failed, return 'Something went wrong...'
    return {
        message: 'Something went wrong...'
    }
}