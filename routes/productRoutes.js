const express = require('express')
const router = express.Router()
const ProductController = require('../controllers/ProductController')
const auth = require('../auth')

// Create single product
router.post("/create", auth.verify, (request, response) => {
	const data = {
		product: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}
	ProductController.addProduct(data).then((result) => {
		response.send(result)
	})
})

// Get all products
router.get('/', (request, response) => {
	ProductController.getAllProducts().then((result) => {
		response.send(result)
	})
})

// Get all ACTIVE products
router.get('/active', (request, response) => {
	ProductController.getAllActive().then((result) => {
		response.send(result)
	})
})

// Get single product
router.get('/:productId', (request, response) => {
	ProductController.getProduct(request.params.productId).then((result) => {
		response.send(result)
	})
})

// Update single product
router.patch('/:productId/Update', auth.verify, (request, response) => { ProductController.updateProduct(request.params.productId, request.body).then((result) => {
	response.send(result)
	})
})

// Archive single product
router.get('/:productId/archive', auth.verify, (request, response) => {
	ProductController.getProductArchive(request.params.productId, request.body).then((result) => {
		response.send(result)
	})
})


module.exports = router

