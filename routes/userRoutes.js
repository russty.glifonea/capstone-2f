const express = require('express')
const router = express.Router()
const UserController = require('../controllers/UserController')
const auth = require('../auth')

// Check if email exists
router.post("/check-email", (request, response) => {
	UserController.checkIfEmailExists(request.body).then((result) => {
		response.send(result)
	})
})

//Register new user
router.post("/register", (request, response) => {
	UserController.register(request.body).then((result) => {
		response.send(result)
	})
})

//Login user
router.post("/login", (request, response) => {
	UserController.login(request.body).then((result) =>{
		response.send(result)
	})
})

// Get single user details
router.get("/:id/details", auth.verify, (request, response) => {
	UserController.getUserDetails(request.params.id).then((result) => {
		response.send(result)
	})
})

// Update single user
router.patch('/:userId/Update', auth.verify, (request, response) => { UserController.updateUser(request.params.userId, request.body).then((result) => {
	response.send(result)
	})
})

// Order a user
router.post('/order', auth.verify, (request, response) => {
	let data = {
		userId: request.body.userId,
		productId: request.body.productId
	}

	UserController.order(data).then((result) => {
		response.send(result)
	})
})
module.exports = router