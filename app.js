const express = require('express')
const mongoose = require('mongoose')
const dotenv = require('dotenv')
const cors = require('cors')
const userRoutes = require('./routes/userRoutes')
const productRoutes = require('./routes/productRoutes')

dotenv.config()

const app = express()
const port = 8001

// MongoDB Connection
mongoose.connect(`mongodb+srv://Tateng501152:${process.env.MONGODB_PASSWORD}@cluster0.in9k1rn.mongodb.net/capstone2?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
})
let db = mongoose.connection
db.once('open', () => console.log('Connected to MongoDB!'))
// MongoDB Connection End

//To avoid CORS errors when trying to send request to our server
app.use(cors())
app.use(express.json())
app.use(express.urlencoded({extended: true}))

//Routes
app.use('/users', userRoutes)
app.use('/product', productRoutes)
//Routes End

app.listen(port, () => {
	console.log(`API is now running on localhost:${port}`)
})
